package u02lab.code;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;

public class RandomGeneratorTest {

    public static final int NUMBER_OF_BIT = 10;
    RandomGenerator randomGenerator;
    Optional<Integer> value;
    @org.junit.Before
    public void setUp() throws Exception {
        randomGenerator = new RandomGenerator(NUMBER_OF_BIT);
    }

    @org.junit.Test
    public void ShouldReturnNextValue() {
        value = randomGenerator.next();

        assertFalse(value.equals(Optional.empty()));
    }

    @org.junit.Test
    public void ShouldReturnNextObjectEmpty() {
        cycleNTime(NUMBER_OF_BIT);
        value = randomGenerator.next();

        assertTrue(value.equals(Optional.empty()));
    }

    private void cycleNTime(int n) {
        for(int i= 0; i < n; i++) {
            value = randomGenerator.next();
        }
    }

    @org.junit.Test
    public void ShouldReset() {
        cycleNTime(NUMBER_OF_BIT + 1);
        randomGenerator.reset();
        value = randomGenerator.next();
        assertFalse(value.equals(Optional.empty()));
    }

    @org.junit.Test
    public void ShouldNotOver() {
        randomGenerator.next();
        assertFalse(randomGenerator.isOver());
    }


    @org.junit.Test
    public void ShouldOver() {
        cycleNTime(NUMBER_OF_BIT + 2);
        assertTrue(randomGenerator.isOver());
    }

    @org.junit.Test
    public void ShouldRetrunRemainingList() {
        cycleNTime(NUMBER_OF_BIT-2);
        List<Integer> remainingList =  randomGenerator.allRemaining();
        System.out.println(remainingList.size());
        assertTrue(remainingList.size() == NUMBER_OF_BIT - (NUMBER_OF_BIT - 2));
    }
}