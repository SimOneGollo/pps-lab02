package u02lab.code;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;

public class RangeGeneratorTest {
    public static final int START = 0;
    public static final int STOP = 10;

    RangeGenerator rangeGenerator;
    Optional<Integer> value;

    @org.junit.Before
    public void setUp() throws Exception {
        rangeGenerator = new RangeGenerator(START, STOP);
    }

    @org.junit.Test
    public void ShouldReturnNextValue() {
        value = rangeGenerator.next();
        assertEquals(Optional.of(0), value);
    }

    @org.junit.Test
    public void ShouldReturnNextObjectEmpty() {

        cycleNTime(STOP+2);
        value = rangeGenerator.next();
        //assertEquals(Optional.empty(), value);
        assertTrue(value.equals(Optional.empty()));
    }

    private void cycleNTime(int n) {
        for(int i= 0; i < n; i++) {
            value = rangeGenerator.next();
        }
    }

    @org.junit.Test
    public void ShouldReset() {
        rangeGenerator.next();
        rangeGenerator.reset();
        value = rangeGenerator.next();
        assertEquals(Optional.of(0), value);
    }

    @org.junit.Test
    public void ShouldNotOver() {
        rangeGenerator.next();
        assertFalse(rangeGenerator.isOver());
    }

    @org.junit.Test
    public void ShouldOver() {
        cycleNTime(STOP+2);
        assertTrue(rangeGenerator.isOver());
    }
    @org.junit.Test
    public void ShouldRetrunRemainingList() {
        List<Integer> expectedList = new ArrayList<Integer>();
        expectedList.addAll(Arrays.asList(8,9,10));
        cycleNTime(STOP-2);

        assertEquals(expectedList, rangeGenerator.allRemaining());
    }
}