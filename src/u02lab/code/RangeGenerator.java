package u02lab.code;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.OptionalInt;

/**
 * Step 1
 *
 * Using TDD approach (create small test, create code that pass test, refactor to excellence)
 * implement the below class that represents the sequence of numbers from start to stop included.
 * Be sure to test that:
 * - the produced elements (called using next(), go from start to stop included)
 * - calling next after stop leads to a Optional.empty
 * - calling reset after producing some elements brings the object back at the beginning
 * - isOver can actually be called in the middle and should give false, at the end it gives true
 * - can produce the list of remaining elements in one shot
 */
public class RangeGenerator implements SequenceGenerator {

    Optional<Integer> value = Optional.empty();

    int start;
    int stop;

    public RangeGenerator(int start, int stop){
        this.value = Optional.of(start);
        this.start = start;
        this.stop = stop;
    }

    @Override
    public Optional<Integer> next() {
        Optional<Integer> tempory = value;
        value = Optional.of(value.get()+1);
        return tempory.get() > stop ? Optional.empty() : tempory ;
    }

    @Override
    public void reset() {
        value = Optional.of(this.start);
    }

    @Override
    public boolean isOver() {
        return value.get() > stop ? true : false;
    }

    @Override
    public List<Integer> allRemaining() {
        List<Integer> remainingList = new ArrayList<>();
        for (int i = value.get(); i <= stop; i++){
            remainingList.add(i);
        }
        return remainingList;
    }
}
