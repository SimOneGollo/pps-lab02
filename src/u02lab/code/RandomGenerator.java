package u02lab.code;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Step 2
 *
 * Now consider a RandomGenerator. Using TDD approach (create small test, create code that pass test, refactor
 * to excellence) implement the below class that represents a sequence of n random bits (0 or 1). Recall
 * that math.random() gives a double in [0,1]..
 * Be sure to test all that is needed, as before
 *
 * When you are done:
 * A) try to refactor the code according to DRY (RandomGenerator vs RangeGenerator), using patterns as needed
 * - be sure tests still pass
 * - refactor the test code as well
 * B) create an abstract factory for these two classes, and implement it
 */
public class RandomGenerator implements SequenceGenerator {

    Optional<Integer> value = Optional.empty();
    List<Optional<Integer>> bitList = new ArrayList<>();
    int numberOfBit = 0;
    int index = 0;

    public RandomGenerator(int n){
        numberOfBit = n;
        for(int i = 0; i < numberOfBit; i++){
            bitList.add(Optional.of(Math.random()>0.5?1:0));
        }
    }

    @Override
    public Optional<Integer> next() {
        int temporaryIndex = index;
        index++;
        return temporaryIndex >= numberOfBit ? Optional.empty(): bitList.get(temporaryIndex);
    }

    @Override
    public void reset() {
        index = 0;
    }

    @Override
    public boolean isOver() {
        return index >= numberOfBit ? true : false;
    }

    @Override
    public List<Integer> allRemaining() {
        List<Integer> tempList = new ArrayList<>();
        bitList.subList(index, numberOfBit).stream().forEach((i)->{
            tempList.add(i.get());
        });
        return tempList;
    }
}
